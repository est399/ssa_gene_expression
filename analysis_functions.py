# Author: Nik Rom (n.rom@ncl.ac.uk / est399@me.com)
# Work period: 1st March - 25th August, 2017

import collections
import numpy as np
from shapely.geometry import *
import pylab as plt
import pandas as pd
import seaborn as sns; sns.set(color_codes=True)
import geopandas as gpd
from detect_peaks import detect_peaks
import warnings


# --<< Some useful stuff now >>--
def correct_data_entries():
    """
    Correct data entries in case there is an error with input.
    p.e. There were no parenthesis around the parameters in the output file.
    :return: corrected file 
    """
    if file is False:
        file = "~/working_dir/output/c1_T20.0.csv"
    with open(file, 'r') as csvfile:
        file_lines = []
        for x in csvfile.readlines():
            x = x.replace('(', '"(').replace(')', ')"')
            file_lines.append(x)

    with open(file, 'w') as sss:
        sss.writelines(file_lines)


# --<< Processing >>--
def process_data(data_initial, normalise=False):
    """
    Data pre-processing in order to return scaled data between [0, 1], as well as calls on the df_values
    in order to calculate values that are of interest to us (height, mode, minX, width).
    :param data_initial: Initial data (read straight from the file)
    :return: normalised values data frame; data frame with values of interest
    """
    # --<< Pre-processing >>--  ''''''' REMOVE this PART @@@@@@@@
    for n in data_initial['param']:

        if n[0] == '(':
            data_initial['param'] = data_initial['param'].str.strip('()')
        if n[0] == '[':
            data_initial['param'] = data_initial['param'].str.strip('[]')
    data_df = data_initial.pivot_table(index=['Trange'], columns=['param', 'run_n'], values='Prange')
    data_df = data_df.mean(level=0, axis=1)

    if normalise is True:
        table_op_norm_mean = (data_df - data_df.min().min()) / (data_df.max().max() - data_df.min().min())
        par_df = df_values(table_op_norm_mean)
        return table_op_norm_mean, par_df

    if normalise is False:
        par_df = df_values(data_df)
        return data_df, par_df


def df_values(df_):
    """
    Create a data frame containing the values of interest (height, mode, minX, width).
    :param df_: normalised data from process_data function
    :return: data frame with values of interest
    """

    par_df = pd.DataFrame(columns=['param', 'height', 'width', 'minX', 'mode'])
    for column in df_:
        s = np.where(df_[column] > 0)

        height = max(df_[column].values[s[0]])
        pos_height = list(df_[column].values[s[0]]).index(height)
        mode = df_[column].index[s[0]][pos_height]
        minX = df_[column].index[s[0][0]]
        width = df_[column].index[s[0][-1]] - minX

        product = sum((df_[column].values[s[0]]*df_[column].index[s[0]]).tolist())
        # meadian = np.median(df_[column].index[s[0]])
        # par = pd.DataFrame([(column, height, width, minX, meadian)],
        #                    columns=['param', 'height', 'width', 'minX', 'median'])
        par = pd.DataFrame([(column, height, width, minX, mode, product)],
                           columns=['param', 'height', 'width', 'minX', 'mode', 'product'])
        par_df = pd.concat([par_df, par])

    par_df.reset_index(inplace=True)
    par_df.drop(['index'], axis=1, inplace=True)
    return par_df.sort_values(by=['param'])


def k_index_dict(dicti, k_i, k_v):
    """
    Find the index numbers of the parameters that fit the conditions and add values to the dictionary.
    :param dicti: initial dictionary with data frames, cam also be a series of data
    :param k_i: value index (p.e. k1 = 0, k_1 = 1, etc.)
    :param k_v: value of the reaction rate of interest (p.e. K1 = 0.005)
    :return: if dicti is a dict_norm - new entry to the intial dictionary with specific data frame
             if dicti is not a dict_norm - return a dictionary containing the calues 
    """
    if dicti is dict:
        seri = (dicti.values()[0])

    else:
        seri = dicti
        dicti = []

    k_titles = ['k1', 'k_1', 'k2', 'k3', 'k4', 'k5', 'k6']
    columns = ['param', 'height', 'width', 'minX', 'median']

    if list(seri.columns)[0] == 'param':
        seri_param = seri['param']
        ttp = 0

    if list(seri.columns)[0] != 'param':
        seri_param = seri.columns
        ttp = 1

    # write out a list of lists containing all the param
    paramlist = []
    for i in seri_param:
        temp = [float(n) for n in i[1:-1].split(',')]
        paramlist.append(temp)

    # Index all the lists(param) that suffice the input param.
    k_index = []

    if type(k_i) == list or type(k_v) == list:
        if type(k_v) == list and type(k_i) == list:
            if len(k_v) == len(k_i):
                pass
            if len(k_v) > len(k_i):
                k_i = k_i * len(k_v)
            if len(k_i) > len(k_v):
                k_v = k_v * len(k_i)
        if type(k_v) == float:
            k_v = [k_v] * len(k_i)

        if type(k_i) == int:
            k_i = [k_i] * len(k_v)

        for o in range(len(k_i)):
            tk_i = k_i[o]
            tk_v = k_v[o]
            for i in range(len(paramlist)):
                if paramlist[i][tk_i] == tk_v:
                    k_index.append(i)

    if type(k_i) == int and type(k_v) == float:
        for i in range(len(paramlist)):
            if paramlist[i][k_i] == k_v:
                k_index.append(i)

    seen = set()
    uniq = []
    for x in k_index:
        if x not in seen:
            uniq.append(x)
            seen.add(x)
    k_index = uniq

    # Create a data frame coherent the conditions.
    if ttp == 0:
        vali = seri.iloc[k_index]
        valititle = vali['param']
    if ttp == 1:
        vali = seri[k_index]
        valititle = pd.Series(vali.columns)


    # Find the values that change through the simulation - works for more than one.
    i_i = (valititle.index.tolist())
    i_i1 = i_i[-1]
    i_i2 = i_i[1]
    temp1 = [float(n) for n in valititle[i_i2][1:-1].split(',')]
    temp2 = [float(n) for n in valititle[i_i1][1:-1].split(',')]
    lst = [temp1[x] - temp2[x] for x in range(len(temp1))]
    param_n = [i for i, e in enumerate(lst) if e != 0]
    param_k_titles = [k_titles[param_n[i]] for i in range(len(param_n))]

    # If there is only one parameters change is observed, the name is writen as a string
    if len(param_n) > 1:
        if type(k_i) == list:
            k_k_titles = [k_titles[k_i[i]] for i in range(len(param_n))]
            dicti['range of {} with {}={}'.format(param_k_titles, k_k_titles, k_v)] = vali

        if type(k_i) == int:
            dicti['range of {} with {}={}'.format(param_k_titles, k_titles[k_i], k_v)] = vali

    elif len(param_n) == 1 and type(k_i) == int:
        dicti['range of {} with {}={}'.format(k_titles[param_n[0]], k_titles[k_i], k_v)] = vali


def extract_unique_par(data_initial, name=False):
    """
    Extracts parameter values that change and records them in a dictionary.
    Further used in order to build SM.
    :param data_initial: initial data loaded from a file
    :param name: if True will return dictionary with names of the parameters,
    index of the parameter otherwise (i.e. k1 = 0, k_1 = 1).
    :return: dictionary containing parameter value position (key) and values (value)
    """
    unique_param = np.unique(data_initial['param'].values)
    columns = ['k1', 'k_1', 'k2', 'k3', 'k4', 'k5', 'k6']

    a = pd.DataFrame(columns=columns)
    if unique_param[0][0] == '(' or unique_param[0][0] == '[':
        for v in unique_param:
            lll = [float(n) for n in v[1:-1].split(',')]
            ldf = pd.DataFrame([lll], columns=columns)
            a = a.append(ldf)
    else:
        for v in unique_param:
            lll = [float(n) for n in v.split(',')]
            ldf = pd.DataFrame([lll], columns=columns)
            a = a.append(ldf)
    uni_par = {}

    if name is False:
        for i in range(len(columns)):
            if len(np.unique(list(a[columns[i]]))) > 1:
                uni_par[i] = np.unique(list(a[columns[i]]))

    elif name is True:
        for i in columns:
            if len(np.unique(list(a[i]))) > 1:
                uni_par[i] = np.unique(list(a[i]))

    return uni_par


def heatmap_df(par_df, uniq_param_dic, four_d=False, orientation=False, product=False):
    """
    Creates a data frame with main parameters as the values. 
    :param par_df: data frame containing all the parameters
    :param uniq_param_dic: dictionary containing parameter value position (key) and values (value)
    :param four_d: Specification for the 4D data frames.
    :return: data frame with columns/index being parameter (p.e. k1/k_1 and values being values of interest.
    """

    # --<< no point in proceeding >>--
    if len(uniq_param_dic.keys()) == 1:
        raise ValueError('Only one parameter changing. No point in making a DataFrame and heatmap')

    # if len(uniq_param_dic.keys()) > 2 and four_d is False:
    #     raise ValueError('More than 2 parameters changing. No point for dataframe and '
    #                      'hence heatmap, consider 4D plots instead. If in progress of making 4D - '
    #                      'then specify it!')

    # --<< in case the decision was made to keep titles as str >>--
    titles = ['k1', 'k_1', 'k2', 'k3', 'k4', 'k5', 'k6']
    if uniq_param_dic.keys()[0] is str:
        param_keys = [titles.index(i) for i in uniq_param_dic.keys()]
    else:
        param_keys = uniq_param_dic.keys()

    param_val = uniq_param_dic.values()

    if orientation is False:
        param_val[0] = sorted(param_val[0], reverse=True)
        param_val[1] = sorted(param_val[1])

    if orientation is True:
        param_val[0] = sorted(param_val[0])
        param_val[1] = sorted(param_val[1])

    # --<< Creating 3 2d data frames >>--
    if len(param_keys) == 3:

        warnings.warn("Creating a data frame with data where more than 2 parameters are changing. \n"
                      "First and second parameters from the list will be used, as well as three entries from"
                      "the third one!")

        def moll(lst):
            n = len(lst)
            if n < 1:
                return None
            if n % 2 == 0:
                return lst[n / 2]
            else:
                return lst[n / (2-1)]

        # df_height = pd.DataFrame(index=param_val[0], columns=param_val[1])
        # df_height.index.name = '{}'.format(param_keys)
        #
        # df_width = pd.DataFrame(index=param_val[0], columns=param_val[1])
        # df_width.index.name = '{}'.format(param_keys)
        #
        # df_minX = pd.DataFrame(index=param_val[0], columns=param_val[1])
        # df_minX.index.name = '{}'.format(param_keys)
        #
        # df_mode = pd.DataFrame(index=param_val[0], columns=param_val[1])
        # df_mode.index.name = '{}'.format(param_keys)
        #
        # if product is True:
        #     df_mode = pd.DataFrame(index=param_val[0], columns=param_val[1])
        #     df_mode.index.name = '{}'.format(param_keys)

        std_val = [1., 50., 500., 50., 10., 1.0, 50.]

        df_param_dic = {}

        par_df = split_param(par_df)

        c1 = titles[param_keys[0]]
        c2 = titles[param_keys[1]]
        c3 = titles[param_keys[-1]]

        vv1 = sorted(list(np.unique(par_df[c1])), reverse=True)
        vv2 = sorted(list(np.unique(par_df[c2])))
        vv3 = sorted(list(np.unique(par_df[c3])))

        cc3_n = vv3[0::5] + [vv3[-1]]
        cc3_n = [vv3[( len(vv3)-len(vv3)*n//4 )] for n in [1, 2, 3, 4]] + [vv3[-1]]

        df_height = pd.DataFrame(index=vv1, columns=vv2)
        df_height.index.name = '{}'.format(param_keys)

        df_width = pd.DataFrame(index=vv1, columns=vv2)
        df_width.index.name = '{}'.format(param_keys)

        df_minX = pd.DataFrame(index=vv1, columns=vv2)
        df_minX.index.name = '{}'.format(param_keys)

        df_mode = pd.DataFrame(index=vv1, columns=vv2)
        df_mode.index.name = '{}'.format(param_keys)

        df_product = pd.DataFrame(index=vv1, columns=vv2)
        df_product.index.name = '{}'.format(param_keys)

        for n in cc3_n:
            par_df_splt = par_df.loc[par_df[c3] == n]
            for rows in par_df_splt.iterrows():
                cc2 = rows[1][c1]
                cc1 = rows[1][c2]
                df_height[cc1][cc2] = rows[1]['height']
                df_width[cc1][cc2] = rows[1]['width']
                df_minX[cc1][cc2] = rows[1]['minX']
                df_mode[cc1][cc2] = rows[1]['mode']
                df_product[cc1][cc2] = rows[1]['product']

            df_param_dic['height - {0}={1}'.format(c3, round(n, 3))] = df_height[df_height.columns].astype(float)
            df_param_dic['width - {0}={1}'.format(c3, round(n, 3))] = df_width[df_width.columns].astype(float)
            df_param_dic['minX - {0}={1}'.format(c3, round(n, 3))] = df_minX[df_minX.columns].astype(float)
            df_param_dic['mode - {0}={1}'.format(c3, round(n, 3))] = df_mode[df_mode.columns].astype(float)
            df_param_dic['product - {0}={1}'.format(c3, round(n, 3))] = df_mode[df_mode.columns].astype(float)

    if len(param_keys) == 2:

        df_height = pd.DataFrame(index=param_val[0], columns=param_val[1])
        df_height.index.name = '{}'.format(param_keys)

        df_width = pd.DataFrame(index=param_val[0], columns=param_val[1])
        df_width.index.name = '{}'.format(param_keys)

        df_minX = pd.DataFrame(index=param_val[0], columns=param_val[1])
        df_minX.index.name = '{}'.format(param_keys)

        df_mode = pd.DataFrame(index=param_val[0], columns=param_val[1])
        df_mode.index.name = '{}'.format(param_keys)

        df_param_dic = {}
        if par_df['param'][0][0] == '(' or par_df['param'][0][0] == '[':
            for rows in par_df.iterrows():
                param = (rows[1])['param']
                param = ([float(n) for n in param[1:-1].split(',')])

                (cc2, cc1) = (param[param_keys[0]], param[param_keys[1]])
                df_height[cc1][cc2] = rows[1]['height']
                df_width[cc1][cc2] = rows[1]['width']
                df_minX[cc1][cc2] = rows[1]['minX']
                df_mode[cc1][cc2] = rows[1]['mode']

        else:
            for rows in par_df.iterrows():
                param = (rows[1])['param']
                param = ([float(n) for n in param.split(',')])

                (cc2, cc1) = (param[param_keys[0]], param[param_keys[1]])
                df_height[cc1][cc2] = rows[1]['height']
                df_width[cc1][cc2] = rows[1]['width']
                df_minX[cc1][cc2] = rows[1]['minX']
                df_mode[cc1][cc2] = rows[1]['mode']

        df_param_dic['height'] = df_height[df_height.columns].astype(float)        # It's important to record as float
        df_param_dic['width'] = df_width[df_width.columns].astype(float)
        df_param_dic['minX'] = df_minX[df_minX.columns].astype(float)
        df_param_dic['mode'] = df_mode[df_mode.columns].astype(float)

    return df_param_dic


def split_param(par_df):
    """
    Splits the param column into 7 columns 
    :param df_initial: data frame with values of interest
    :return: data frame with split param column for later use along with unique_par
    """
    if par_df['param'][0][0] == '(':
        df = pd.concat([pd.DataFrame(par_df['param'].str.strip('()').str.split(',').tolist(),
                                     columns=['k1', 'k_1', 'k2', 'k3', 'k4', 'k5', 'k6']),
                        par_df.drop('param', axis=1)], axis=1)

    if par_df['param'][0][0] == '[':
        df = pd.concat([pd.DataFrame(par_df['param'].str.strip('[]').str.split(',').tolist(),
                                     columns=['k1', 'k_1', 'k2', 'k3', 'k4', 'k5', 'k6']),
                        par_df.drop('param', axis=1)], axis=1)

    else:
        df = pd.concat([pd.DataFrame(par_df['param'].str.split(',').tolist(),
                                     columns=['k1', 'k_1', 'k2', 'k3', 'k4', 'k5', 'k6']),
                        par_df.drop('param', axis=1)], axis=1)

    df = df[df.columns].astype(float)

    return df


# --<< Plotting >>--
def df_to_shapely(op_nm):
    """
    Convert values from a data frame to shapely point for later use for plotting.
    :param op_nm: data frame containing the values 
    :return: dictionary with keys being the parameter and values - points
    """
    point_dict = {}
    for column in op_nm:
        temp_point1 = []
        temp_point2 = []

        for n in range(len(op_nm[column])):

            if op_nm[column].values[n] > 0.:
                temp_point1.append(op_nm[column].index[n])
                temp_point2.append(op_nm[column].values[n])
            else:
                pass

        # --<< Add another point to start/finish at zero >>--
        if temp_point2[-1] != 0.0:
            temp_point1.append(temp_point1[-1])
            temp_point2.append(0.0)
        if temp_point2[0] != 0.0:
            temp_point1.insert(0, temp_point1[0])
            temp_point2.insert(0, 0.0)

        temp_point = zip(temp_point1, temp_point2)
        point_dict['{}'.format(column)] = temp_point

    return point_dict


def set_sm(key_val, cmap):
    """
    Set up the color gradient for the color bar using the param dict_norm
    :param key_val: 
    :return: 
    """

    MIN = min(key_val)
    MAX = max(key_val)

    sm = plt.cm.ScalarMappable(cmap=cmap, norm=plt.Normalize(vmin=MIN, vmax=MAX))
    sm.set_array(key_val)

    return sm


def plot_polygon(ax, sm, point_dict, k, every_nth, title='k'):
    """
    Plot polygon
    :param ax: axis
    :param sm: preset colormap 
    :param point_dict: dictionary containing all the Shapely points
    :param k: unique parameter index
    :param every_nth: if specified - plot every Nth
    :param title: title of the plot
    :return: plot object
    """

    area = {}
    bounds = {}

    if every_nth == 0:
        for keys, values in point_dict.iteritems():

            temp = [float(n) for n in keys[1:-1].split(',')]

            lin_ring = LinearRing(values)
            polygon = Polygon(lin_ring)
            x, y = polygon.exterior.xy

            area[keys] = polygon.area
            bounds[keys] = polygon.bounds

            ax.plot(x, y, alpha=0.7, linewidth=1, solid_capstyle='round',
                    zorder=2, color=sm.to_rgba(temp[k]))

            ax.set_title('{} value distribution'.format(title))


    else:
        for i in range(0, len(point_dict.keys()), every_nth):
            keys = point_dict.keys()[i]

            temp = [float(n) for n in keys[1:-1].split(',')]

            lin_ring = LinearRing(point_dict[keys])
            polygon = Polygon(lin_ring)
            x, y = polygon.exterior.xy

            area[keys] = polygon.area
            bounds[keys] = polygon.bounds

            ax.plot(x, y, alpha=0.7, linewidth=1, solid_capstyle='round',
                    zorder=2, color=sm.to_rgba(temp[k]))

            ax.set_title('{0} value distribution (every {1}th)'.format(title, every_nth))

    plt.tight_layout()
    return (area, bounds)


def plot_distribution(point_dict, uniq_param_dic, every_nth=0):
    """
    Utilises plot_polygon function to plot distribution
    :param point_dict: point dictionary
    :param uniq_param_dic: parameter dictionary
    :param every_nth: if specified - plot every Nth
    :return: plot
    """
    fig, ax = plt.subplots(len(uniq_param_dic.keys()), 1)
    par_titles = ['k1', 'k_1', 'k2', 'k3', 'k4', 'k5', 'k6']
    plot_title = ', '.join([par_titles[k] for k in uniq_param_dic.keys()])

    c = 0

    for k, v in uniq_param_dic.iteritems():
        parm_name = par_titles[k]

        # Set colorbar
        cmap = plt.cm.get_cmap('jet')
        sm_ = set_sm(v, cmap)

        if len(uniq_param_dic.keys()) > 1:
            cb = plt.colorbar(sm_, ax=ax[c])
            cb.ax.set_title(parm_name)
            (area, bounds) = plot_polygon(ax[c], sm_, point_dict, k, every_nth, title=plot_title)

        if len(uniq_param_dic.keys()) == 1:
            cb = plt.colorbar(sm_, ax=ax)
            cb.ax.set_title(parm_name)
            (area, bounds) = plot_polygon(ax, sm_, point_dict, k, every_nth, title=plot_title)

        c += 1

    return (area, bounds)


def plot_heatmap(df_param_dic, accurate=False, four_param=False, annot=False,
                 plt_norm=False, norm_dict=None):
    """
    Calls on 'df_dic_param_all' function to create data frames dictionary for main values of
    interest. Further uses seaborn to plot heatmaps. note: if less, or more than 2 parameters
    undergo change, returns an error.
    :param par_df: data frame containing all the parameters
    :param uniq_param_dict: dictionary containing unique parameters
    :param accurate: how accurate the heatmap will be (changes the scale)
    :return: heatmap
    """
    titles = ['k1', 'k_1', 'k2', 'k3', 'k4', 'k5', 'k6']
    nnames = map(int, df_param_dic.values()[0].index.name[1:-1].split(','))

    nnames = [titles[i] for i in nnames]
    c = 0
    # if len(df_param_dict.keys()) == 4:
    #     cc = 221
    #
    # if len(df_param_dict.keys()) == 12:
    #     cc = 341
    #
    # else:
    #     cc = 221

    if four_param is True:
        klist= chunks(sorted(df_param_dic.keys()))

        for ck in klist:
            fig = plt.figure()
            fig.tight_layout()
            ck.sort(key=lambda x: x.split('- k')[1][3])
            vv = []
            vvm = []
            for k in ck:
                v = df_param_dic[k]
                vv.append(v.max().max())
                vvm.append(v.min().min())
            v = max(vv)
            vm = min(vvm)
            i = 0
            axxs = []
            for k in ck:
                v = df_param_dic[k]
                yyticks = list(v.index)
                xxticks = list(v.columns)
                if yyticks[-1] < 5.:
                    yyticks = [round(n, 3) for n in yyticks]
                elif yyticks[-1] < 20.:
                    yyticks = [round(n, 2) for n in yyticks]
                elif yyticks[-1] >= 100.:
                    yyticks = [round(n, 1) for n in yyticks]

                if xxticks[-1] < 5.:
                    xxticks = [round(n, 3) for n in xxticks]
                elif xxticks[-1] < 20.:
                    xxticks = [round(n, 2) for n in xxticks]
                elif xxticks[-1] >= 100.:
                    xxticks = [round(n, 1) for n in xxticks]

                ax = fig.add_subplot(1, len(ck), 1+i)
                fig.subplots_adjust(top=0.88)

                axxs.append(ax)
                i += 1
                norm = plt.Normalize(vmin=vm, vmax=v)
                # plt.subplots(131+c)
                if annot is True:
                    annott = True
                elif annot is False:
                    annott = False
                if k[:7] == 'height':
                    im = sns.heatmap(v,#annot=annott,fmt='.0%', annot_kws={"size": 4},
                                     cbar=False, ax=ax,
                                     cmap='coolwarm', norm=norm,#vmin=vm, vmax=v,
                                     yticklabels=yyticks, xticklabels=xxticks)
                elif k[:7] != 'height':
                    if accurate is False:
                        im = sns.heatmap(v, #annot=annott, fmt='.0%', annot_kws={"size": 4},
                                         cbar=False, ax=ax, norm=norm, #vmin=vm, vmax=v,
                                         yticklabels=yyticks, xticklabels=xxticks)  # , cmap='coolwarm', vmin=0, yticklabels=yyticks)
                    if accurate is True:
                        vmax = max([df_param_dic.values()[i].max().max() for i in range(len(df_param_dic.values()))])
                        im = sns.heatmap(v, #annot=annott, fmt='.0%', annot_kws={"size": 4},
                                         cbar=False, ax=ax,
                                         cmap='coolwarm', norm=norm, #vmin=0, vmax=v,
                                         yticklabels=yyticks, xticklabels=xxticks)
                # print "hello"
                if k == ck[-1]:
                    mappable = im.get_children()[0]
                #     # pc = ax.pcolor(vmin=vm, vmax=v)
                    plt.colorbar(mappable, ax=ax)
                plt.ylabel(nnames[0])
                plt.xlabel(nnames[1])

                if k != ck[0]:
                    # im.set(yticks=[])
                    # im.set(xticks=[])
                    # im.set(yticklabels=[])
                    # plt.ylabel([])
                    # plt.xlabel([])
                    ax.get_yaxis().set_visible(False)
                    # ax.get_xaxis().set_visible(False)

                plt.yticks(rotation=0)
                plt.xticks(rotation=90)
                plt.xlabel(nnames[1], rotation=0)

                plt.title(k.split('- ')[1])
                a = plt.suptitle(k.split(' -')[0])

                if len(yyticks) > 21:
                    plt.locator_params(axis='y', tight=True, nbins=20)

                plt.tick_params(labelsize=5)
                c += 1
            fig.subplots_adjust(left=0.03, bottom=0.17, right=0.97,
                                top=0.84, wspace=0.03)
    if four_param is False:
        if len(df_param_dic.keys()) > 4:
            warnings.warn('Too many values, check record type, consider four_param setting')
        plt.figure()

        for k, v in df_param_dic.iteritems():
            norm = plt.Normalize(vmin=norm_dict[k][0], vmax=norm_dict[k][1])
            print norm_dict[k], k
            yyticks = list(v.index)
            xxticks = list(v.columns)

            if yyticks[-1] < 5.:
                yyticks = [round(n, 3) for n in yyticks]
            elif yyticks[-1] < 20.:
                yyticks = [round(n, 2) for n in yyticks]
            elif yyticks[-1] >= 100.:
                yyticks = [round(n, 1) for n in yyticks]

            if xxticks[-1] < 5.:
                xxticks = [round(n, 3) for n in xxticks]
            elif xxticks[-1] < 20.:
                xxticks = [round(n, 2) for n in xxticks]
            elif xxticks[-1] >= 100.:
                xxticks = [round(n, 1) for n in xxticks]

            plt.subplot(221 + c)

            if k[:7] == 'height':
                if plt_norm is True:
                    sns.heatmap(v, cmap='coolwarm', vmin=0,
                                norm=norm, yticklabels=yyticks, xticklabels=xxticks)
                else:
                    sns.heatmap(v, cmap='coolwarm', vmin=0,
                                yticklabels=yyticks, xticklabels=xxticks)
            else:
                if accurate is False:
                    if plt_norm is True:
                        sns.heatmap(v, yticklabels=yyticks, xticklabels=xxticks,
                                    norm=norm)#, cmap='coolwarm', vmin=0, yticklabels=yyticks)
                    else:
                        sns.heatmap(v, yticklabels=yyticks, xticklabels=xxticks)
                if accurate is True:
                    vmax = max([df_param_dic.values()[i].max().max() for i in range(len(df_param_dic.values()))])
                    sns.heatmap(v, cmap='coolwarm', vmin=0, vmax=vmax, yticklabels=yyticks, xticklabels=xxticks)

            a = plt.ylabel(nnames[0], rotation=0)

            plt.yticks(rotation=0)
            plt.xticks(rotation=90)
            plt.xlabel(nnames[1], rotation=0)

            plt.title(k)

            if len(yyticks) > 21:
                plt.locator_params(axis='y', tight=True, nbins=20)

            plt.tick_params(labelsize=7)
            c += 1

    plt.tight_layout()


def chunks(l, n=5):
    """Yield successive n-sized chunks from l."""

    # if len(l) % 12 == 0:
    #     n = 3
    # if len(l) % 16 == 0:
    #     n = 4
    if len(l) % 20 == 0:
        n = 5
    for i in xrange(0, len(l), n):
        yield l[i:i + n]


def plot_par_df(seri, title, label=False):
    paramlist = []
    for i in seri['param']:
        temp = [float(n) for n in i[1:-1].split(',')]
        paramlist.append(temp)

    lst = [paramlist[2][x] - paramlist[1][x] for x in range(len(paramlist[1]))]
    param_n = [x for x in range(len(lst)) if lst[x] != 0][0]

    seri_list = list(seri.columns)[1:]

    par_titles = ['k1', 'k_1', 'k2', 'k3', 'k4', 'k5', 'k6']
    seri = split_param(seri)
    seri = seri.sort_values(by=par_titles[param_n])

    jet = cm = plt.get_cmap('jet')
    cNorm = plt.Normalize(vmin=0, vmax=7)
    scalarMap = plt.cm.ScalarMappable(norm=cNorm, cmap=jet)
    colorVal = scalarMap.to_rgba(param_n)


    for c in range(len(seri_list)):
        plt.subplot(221 + c)
        y = seri[seri_list[c]].tolist()

        # x = ([paramlist[n][param_n] for n in range(len(paramlist))])
        x = np.linspace(0, len(y), len(y))
        # plt.scatter(x, y, marker='.', s=20, label=title)

        plt.plot(x, y, linewidth=1, label=title, c=colorVal)

        xy = ((x[0] - 1), y[0])
        a = plt.annotate(title, xy=xy, fontsize=7)
        a.draggable()
        # z = np.polyfit(x, y, 2)
        # f = np.poly1d(z)
        # # calculate new x's and y's
        # x_new = np.linspace(x[0], x[-1], 50)
        # y_new = f(x_new)
        # plt.plot(x_new, y_new, linewidth=1)

        if label is True:
            plt.ylabel('value', rotation=0, fontsize=10, labelpad=20)
            plt.xlabel('param', fontsize=10)
        plt.title(seri_list[c], style='italic')
        plt.subplots_adjust(hspace=.8, wspace=.5)

        # if c == 0:
        #     plt.ylim([0, 1.05])
        # if c == 1:
        #     plt.ylim([0, 3000])
        # if c == 2:
        #     plt.ylim([0, 2500])
        # if c == 3:
        #     plt.ylim([1, 3500])


def plot_peaks(data_initial, uniq_param_dic, mph=0, mpd=0, print_n=False):
    ccc = 0
    nn1 = 0
    nn2 = 0
    nn3 = 0
    nn4 = 0
    nn5 = 0
    (k1, k2) = (uniq_param_dic.keys()[0], uniq_param_dic.keys()[1])
    df_peaks = pd.DataFrame(index=sorted(uniq_param_dic[k1], reverse=True),
                            columns=sorted(uniq_param_dic[k2]))
    df_peaks.index.name = '{}'.format([k1, k2])

    values_range = []
    gr = data_initial.groupby('param')
    for k, i in gr:
        v = gr.get_group(k)['Prange']
        x = gr.get_group(k)['Trange']
        x.reset_index().drop('index', axis=1)
        ind = detect_peaks(v, mph=mph, mpd=mpd, show=False, valley=False)

        ind = [x.values[i] for i in ind]
        if len(ind) not in values_range:
            values_range.append(len(ind))

        temp = [float(n) for n in k[1:-1].split(',')]
        (cc1, cc2) = temp[k1], temp[k2]
        df_peaks[cc2][cc1] = float(len(ind))

        ccc += 1
        if len(ind) > 1:
            # print k, "\t peaks at ->", ind
            nn1 += 1
        if len(ind) > 2:
            nn2 += 1
        if len(ind) > 3:
            nn3 += 1
        if len(ind) > 4:
            nn4 += 1
        if len(ind) > 5:
            nn5 += 1
            # print len(ind), ind
    df_peaks = df_peaks[df_peaks.columns].astype(float)

    if print_n is True:
        print mph, mpd, "\t\t1:", nn1, "\t2:", nn2, "\t3:", nn3, "\t4:", nn4, "\t>5:", nn5

    return df_peaks


# --<< Not very useful at this time, but was useful on the way >>--
def mean_dict(mean_data):
    """
    Convert normalised mean data frame to dictionary and sort
    """
    op_dict = {}
    for column in mean_data:
        temp = mean_data[column].reset_index()
        op_dict[column] = temp

    op_dict = collections.OrderedDict(sorted(op_dict.items()))
    return op_dict


def dic_values(OP_dict):
    """
    Creates a dictionary containing the values of interest (height, width, minX, maxX, medianX)
    :param OP_dict: ??? dunno
    :return: 
    """
    specs = {}
    specs_var = {'height': [], 'width': [], 'minX': [], 'maxX': [], 'medianX': []}
    specs_var_key = []
    for key, value in OP_dict.iteritems():
        tempkey = [float(n) for n in key[1:-1].split(',')]
        specs_var_key.append(tempkey)

        par = {}
        temp = []
        tempY = value[key].tolist()
        tempX = value['Trange'].tolist()
        for (c) in (range(len(value[key]))):
            if tempY[c] > 0:
                temp.append(tempX[c])
        heightY = max(tempY) - min(tempY)
        widthX = max(temp) - min(temp)
        minX = min(temp)
        maxX = max(temp)
        medianX = np.median(temp)

        par['height'] = heightY
        par['width'] = widthX
        par['minX'] = minX
        par['maxX'] = maxX
        par['medianX'] = medianX
        specs[key] = par

        specs_var['height'] = specs_var['height'] + [heightY]
        specs_var['width'] = specs_var['width'] + [widthX]
        specs_var['minX'] = specs_var['minX'] + [minX]
        specs_var['maxX'] = specs_var['maxX'] + [maxX]
        specs_var['medianX'] = specs_var['medianX'] + [medianX]
    return specs, specs_var, specs_var_key


def dict_to_shapely(op_dict):
    """
    Convert dict_norm entries to Shapely.Point object
    :param op_dict: 
    :return: 
    """
    point_dict = {}
    for keys, values in op_dict.iteritems():
        temp_point1 = []
        temp_point2 = []

        size_non_nan = list((np.isnan(values.ix[:, 1]))).count(False)
        for c in range(size_non_nan):
            if values.ix[:, 1][c] > 0.:
                temp_point1.append(values.ix[:, 0][c])
                temp_point2.append(values.ix[:, 1][c])

        temp_point = zip(temp_point1, temp_point2)
        point_dict['{}'.format(keys)] = temp_point

    return point_dict


def create_grid_df(x_list_t, y_list_t, col_name=False):

    x_list_col = range(len(x_list_t))[1:]
    x_list_col_name = ['{}-{}'.format(float(x), float(x+x_list_t[1])) for x in x_list_t[:-1]]

    if col_name is True:
        x_list_col = x_list_col_name

    y_list_col = [y / 10.0 for y in y_list_t[::-1]][:-1]

    grid_df = gpd.GeoDataFrame(columns=x_list_col,
                               index=y_list_col)

    n_y = 0
    c_y = 0
    yy1 = 0
    while yy1 < max(y_list_t) / 10:
        yy0 = float(y_list_t[n_y]) / 10
        yy1 = float(y_list_t[n_y + 1]) / 10
        n_y += 1
        n_x = 0
        c_x = 0

        xx1 = 0

        while xx1 < max(x_list_t):
            xx0 = float(x_list_t[n_x])
            xx1 = float(x_list_t[n_x + 1])
            n_x += 1

            box_t = box(minx=float(xx0), miny=float(yy0), maxx=float(xx1), maxy=float(yy1))
            if col_name is True:
                grid_df['{}-{}'.format(xx0, xx1)][yy1] = box_t
            elif col_name is False:
                grid_df[c_x + 1][yy1] = box_t
            c_x += 1

        c_y += 1

    return grid_df


def extr_keys(data_dict, parm_n):
    """
    Extract float value of the parameter from the param long sting. (Better version is available)
    :param data_dict: any data dictionary
    :param parm_n: number of the parameter that is of interest
    :return: list of key values
    """
    key_values = []
    for keys, values in data_dict.iteritems():
        newlist = [float(n) for n in keys[1:-1].split(',')]
        newlist = round(newlist[parm_n], 2)
        key_values.append(newlist)

    return key_values


def plot_seaborn_fit(seri):
    """
    :param seri: 
    :return: 
    """
    paramlist = []
    for i in seri['param']:
        temp = [float(n) for n in i[1:-1].split(',')]
        paramlist.append(temp)

    lst = [paramlist[2][x]-paramlist[1][x] for x in range(len(paramlist[1]))]
    param_n = [x for x in range(len(lst)) if lst[x] != 0][0]

    seri_list = list(seri.columns)[1:]
    color = np.random.rand(3, )
    for c in range(len(seri_list)):
        plt.subplot(221 + c)
        y = seri[seri_list[c]].tolist()
        x = ([paramlist[n][param_n] for n in range(len(paramlist))])
        sns.regplot(x=np.array(x), y=np.array(y), marker="+", label='K1', color=color)
        # sns.regplot(x=(np.linspace(0, len(y), len(y))), y=np.array(y), marker="+", label='K1', color=color)
        plt.ylabel('value', rotation=0, fontsize=10, labelpad=20)
        plt.xlabel('param', fontsize=10)
        plt.title(seri_list[c], style='italic')
        plt.subplots_adjust(hspace=.8, wspace=.5)

        if c == 0:
            plt.ylim([0, 1.05])
        if c == 1:
            plt.ylim([0, 3000])
        if c == 2:
            plt.ylim([0, 2500])
        if c == 3:
            plt.ylim([1, 3500])
