# Author: Nik Rom (n.rom@ncl.ac.uk / est399@me.com)
# Work period: 1st March - 25th August, 2017

from ssa_functions import *
import itertools
import time


time_started = time.time()

# --<< Set the default parameters, that can be changed if necessary >>--
# k1 = [1.]       # interval [0.005 - 5.0]
# k_1 = [50.]     # interval [1.0 - 100.0]
# k2 = [500.]     # interval [100.0 - 1000.0]
# k3 = [50.]      # interval [10.0 - 120.0]
# k4 = [10.]      # interval [6.0 - 14.0]
# k5 = [1.0]      # interval [0.1 - 3.0]
# k6 = [50.]      # interval [10.0 - 100.0]


# --<< Set ranges of parameters for appropriate interval, for convenience commented out >>--
# kk1 = np.linspace(0.005, 5.0, 30)
# kk_1 = np.linspace(1.0, 100.0, 30)
# kk2 = np.linspace(100.0, 1000.0, 30)
# kk3 = np.linspace(10.0, 120.0, 30)
# kk4 = np.linspace(6.0, 14.0, 30)
# kk5 = np.linspace(0.1, 3.0, 30)
# kk6 = np.linspace(10.0, 100.0, 30)

# --<< Extreme cases of k1 (binding) exhibit abnormal behaviour, hence a range >>--
# l = range(5001)
# a = l[50::25]
# k1 = [a[x]/10000. for x in range(len(a))]

# ck_list = [kk1, kk_1, kk2, kk3, kk4, kk5, kk6]
# kk_list = [k1, k_1, k2, k3, k4, k5, k6]


def run_k3():
    k3 = [10.0, 15.5, 21.0, 26.5, 32.0, 37.5, 43.0, 48.5, 54.0, 59.5, 65.0,
          70.5, 76.0, 81.5, 87.0, 92.5, 98.0, 103.5, 109.0, 114.5, 120.0]
    kk_list = [k1, k_1, k2, k3, k4, k5, k6]
    comb = list(itertools.product(k1, k_1, k2, k3, k4, k5, k6))
    Type = specify_type(kk_list)
    Type = "fuck_alll"
    Time = 100.
    TF = 1000.
    sample_interval_distribution = 40
    mydir = check_set_dir()  # Check if exists (create) and set directory -> ~/output
    run_n = check_run_n(mydir, Type, Time) + 1  # Check if previous file exists, read the last run number and set it +1

    print "\t\trunning simulation for {}".format(Type)
    timestarted = time.time()
    counter = 0
    while counter < 1:
        run_nn = run_n + counter
        print "Simulation number {}.".format(run_nn)

        timestart = time.time()
        multiprocess_run(comb, TF, Time, mydir, run_nn, sample_interval_distribution, Type, processors_number=2)
        counter += 1

        print ("time spent on simulation {} ".format(time.time() - timestart))

    print ("--- time spent in total on the cycle {} sec ---".format(time.time() - timestarted))


def run(Time=20, runs=1, processor=2, linspace=20,
        kkmm1=False, Typee=None,
        kk1=False, kk_1=False, kk2=False,
        kk3=False, kk4=False, kk5=False, kk6=False):
    k1 = [1.]  # interval [0.005 - 5.0]
    k_1 = [50.]  # interval [1.0 - 100.0]
    k2 = [500.]  # interval [100.0 - 1000.0]
    k3 = [50.]  # interval [10.0 - 120.0]
    k4 = [10.]  # interval [6.0 - 14.0]
    k5 = [1.0]  # interval [0.1 - 3.0]
    k6 = [50.]  # interval [10.0 - 100.0]

    if kkmm1 is True:
        k1 = np.linspace(0.005, 0.250, linspace, endpoint=True)
        Typee = 'mmm'
    if kk1 is True:
        k1 = np.linspace(0.005, 5.0, linspace, endpoint=True)
    if kk_1 is True:
        k_1 = np.linspace(1.0, 100.0, linspace, endpoint=True)
    if kk2 is True:
        k2 = np.linspace(100.0, 1000.0, linspace, endpoint=True)
    if kk3 is True:
        k3 = np.linspace(10.0, 120.0, linspace, endpoint=True)
    if kk4 is True:
        k4 = np.linspace(6.0, 14.0, linspace, endpoint=True)
    if kk5 is True:
        k5 = np.linspace(0.1, 3.0, linspace, endpoint=True)
    if kk6 is True:
        k6 = np.linspace(10.0, 100.0, linspace, endpoint=True)

    kk_list = [k1, k_1, k2, k3, k4, k5, k6]

    # --<< Specify Type >>--
    Type = specify_type(kk_list)

    if Typee is not None:
        Type = Type + Typee

    # --<< Return all the possible combinations of parameters
    comb = list(itertools.product(k1, k_1, k2, k3, k4, k5, k6))

    # --<< Set environment fo the simulation >>--
    Time = Time
    TF = 1000.
    sample_interval_histogram = 100
    sample_interval_distribution = 40
    mydir = check_set_dir()  # Check if exists (create) and set directory -> ~/output

    # Check if previous file exists, read the last run number and set it +1
    run_n = check_run_n(mydir, Type, Time) + 1

    print "\t\trunning simulation for {}".format(Type)
    timestarted = time.time()
    counter = 0
    while counter < runs:
        run_nn = run_n + counter
        print "Simulation number {}.".format(run_nn)

        timestart = time.time()
        multiprocess_run(comb, TF, Time, mydir, run_nn, sample_interval_distribution, Type, processors_number=processor)
        counter += 1

        print ("time spent on simulation {} ".format(time.time() - timestart))

    print ("--- time spent in total on the cycle {} sec ---".format(time.time() - timestarted))

run(runs=1, kkmm1=True, kk_1=False, kk4=True, processor=4)
run(runs=1, kkmm1=True, kk_1=False, kk5=True, processor=4)
run(runs=4, kk1=True, kk_1=False, kk4=True, processor=4)
run(runs=4, kk1=True, kk_1=False, kk5=True, processor=4)
run(runs=4, kkmm1=True, kk_1=False, kk4=True, processor=4)
run(runs=4, kkmm1=True, kk_1=False, kk5=True, processor=4)

# run(runs=5, kk4=True, kk_1=True, processor=4)
# run(runs=5, kk5=True, kk_1=True, processor=4)
# run(runs=5, kk1=True, kk_1=True, kk5=True, processor=4)

def run_indv_list(runs=1):
    kk1 = np.linspace(0.005, 5.0, 30)
    kk_1 = np.linspace(1.0, 100.0, 30)
    kk2 = np.linspace(100.0, 1000.0, 30)
    kk3 = np.linspace(10.0, 120.0, 30)
    kk4 = np.linspace(6.0, 14.0, 30)
    kk5 = np.linspace(0.1, 3.0, 30)
    kk6 = np.linspace(10.0, 100.0, 30)


    for n in range(len(ck_list)):
        kk_list = [k1, k_1, k2, k3, k4, k5, k6]
        kk_list[n] = ck_list[n]

        # --<< Specify Type >>--
        Type = specify_type(kk_list)

        # --<< Return all the possible combinations of parameters
        # comb = list(itertools.product(k1, k_1, k2, k3, k4, k5, k6))
        comb = list(itertools.product(kk_list[0], kk_list[1], kk_list[2], kk_list[3],
                                      kk_list[4], kk_list[5], kk_list[6]))

        # --<< Set environment fo the simulation >>--
        Time = 30.
        TF = 1000.
        sample_interval_histogram = 100
        sample_interval_distribution = 40
        mydir = check_set_dir()                     # Check if exists (create) and set directory -> ~/output

        # Check if previous file exists, read the last run number and set it +1
        run_n = check_run_n(mydir, Type, Time) + 1

        print "\t\trunning simulation for {}".format(Type)
        timestarted = time.time()
        counter = 0
        while counter < runs:
            run_nn = run_n + counter
            print "Simulation number {}.".format(run_nn)

            timestart = time.time()
            multiprocess_run(comb, TF, Time, mydir, run_nn, sample_interval_distribution, Type, processors_number=2)
            counter += 1

            print ("time spent on simulation {} ".format(time.time() - timestart))

        print ("--- time spent in total on the cycle {} sec ---".format(time.time() - timestarted))


