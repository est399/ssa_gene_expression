import os
import time

import pandas as pd

from a_code.Functions import *

# os.chdir("/Users/nikitarom/Bitbucket/final_project/")
# import sys
# sys.path.append(os.getcwd())

K1 = 1.     # binding rate --------------- molecule^(-1) hour^(-1) ------ interval [0.005 - 5.0]
K_1 = 50.   # unbinding rate ------------- hour^(-1) -------------------- interval [1.0 - 100.0]
K2 = 500.   # transcription rate --------- hour^(-1) -------------------- interval [100.0 - 1000.0]
K3 = 50.    # translation rate ----------- hour^(-1) -------------------- interval [10.0 - 120.0]
K4 = 10.    # mRNA degradation rate ------ hour^(-1) -------------------- interval [6.0 - 14.0]
K5 = 1.0    # protein degradation rate --- hour^(-1) -------------------- interval [0.1 - 3.0]
K6 = 50.    # basal transcription rate --- hour^(-1) -------------------- interval [10.0 - 100.0]


TF = 1000. # Number of transcription factors ----- not to be changed during this study.

Time = 20.0 # h

sample_interval_histogram = 100
sample_interval_distribution = 40

time_list = [20, 40, 60, 70, 80, 100, 120, 200]
t_v_dic = {}

time_st = time.time()
time_df = pd.DataFrame()
for t in time_list:
    c = 20
    lt = {}

    while c < 30:
        time_st = time.time()
        (trange,prange) = gill(K1,K_1,K2,K3,K4,K5,K6,TF, t)
        (trangeX, prangeX) = listForUniform(trange,prange)
        (list1b,list2b) = distribution(prangeX,sample_interval_distribution)
        c += 1
        lt[c] = (list1b,list2b)
        ttt = round(time.time()-time_st, 2)
        print "-- {0}h - no. {1} -- spent {2}s --".format(t, c, ttt)
        time_df = time_df.append(pd.DataFrame(zip([t],[c],[ttt]), columns=['h', 'n', 'time']))

    t_v_dic[t] = lt

df = pd.DataFrame()
for ke in range(len(t_v_dic.keys())):
    for k in range(len(t_v_dic.values()[ke].keys())):
        data = t_v_dic.values()[ke].values()[k]
        tim = [t_v_dic.keys()[ke]]*len(data[0])
        kk = [k]*len(data[0])
        tdata = zip(data[0], data[1], kk, tim)
        temp = pd.DataFrame(tdata, columns=['list1', 'list2', 'no','time'])
        df = df.append(temp)
        print ke, k, "-- done"


cwd = os.getcwd()
with open('{}/var'.format(cwd), 'a') as csvfile:
    df.to_csv(csvfile, index=False, header=False)

with open('{}/var_times_1'.format(cwd), 'a') as csvfile:
    time_df.to_csv(csvfile, index=False, header=False)
