import pandas as pd
import numpy as np
from scipy.stats import variation
import peakutils
import pylab as plt


with open('/Users/nikitarom/Bitbucket/final_project/my_code_adaptation/test_time_per/var_times', 'r') as csvfile:
    df_time = pd.read_csv(csvfile, names=['h', 'n', 'time'])


with open('/Users/nikitarom/Bitbucket/final_project/my_code_adaptation/test_time_per/var', 'r') as csvfile:
    df = pd.read_csv(csvfile, names=['list1', 'list2', 'no','time'])

df_table = df.pivot_table(index='list1', values='list2', columns=['time', 'no'])

df_time_t = df_time.pivot_table(values='time', columns=['h','n'])
l_time = list(df_table.columns.get_level_values(0).unique())


columns = ['simulation time', 'min-x', 'mode', 'time spent', 'width CV', 'height CV']    # , 'peak_n', 'peak_var']
df = pd.DataFrame(columns=columns)

val = {}
for i in l_time:
    data = df_table[i]
    # print np.mean(data.var3(axis=1))
    width = []
    height = []
    indx = []
    minxxx = []
    modeee = []
    for o in data.columns:
        ntn = (np.where(data[o] > 0))
        we = ntn[0][-1] - ntn[0][0]
        he = data[o].max() - data[o].min()
        minx = data[o].index[ntn[0][0]]
        width.append(we)
        height.append(he / i)
        minxxx.append(minx)
        mode = data[o].idxmax()
        modeee.append(mode)
        #ind = peakutils.indexes(data[o].dropna())
        #indx.append(len(ind))
    l = [i, df_time_t[i].mean(), variation(minxxx)*100, variation(modeee)*100, variation(width)*100, variation(height)*100]   # , np.mean(indx), np.var(indx)]

    columns = ['simulation time', 'time spent', 'min-x CV', 'mode CV', 'width CV', 'height CV']                 # , 'peak_n', 'peak_var']
    pddft = pd.Series(l, index=columns)
    df = df.append(pddft, ignore_index=True)

    val[i] = {'time': df_time_t[i].tolist(), 'width': width, 'height': height}  #, 'peaks': indx}
    print "\a", i, "\t time:", df_time_t[i].mean(), \
        "\t minx_var:", round(variation(minxxx), 3)*100, \
        "\t mode_var:", round(variation(modeee), 3)*100, \
        "\t we_var:", round(variation(width), 3)*100, \
        "\t he_var:", round(variation(height), 3)*100                       # , \
        #"\t pe_n:", np.mean(indx), "\t pe_var:", round(np.var(indx), 3)


df = df.set_index('simulation time')

df_norm = (df) # ( - df.min()) / (df.max() - df.min())

i = 0
fig, ax = plt.subplots()
# for ii in df_norm.columns:
#     if ii is 'time spent':
#         df_norm[ii].plot(ax=ax2)
#
#     # plt.subplot(231+i)
#     if ii is not 'time spent':
#         df_norm[ii].plot(ax=ax)
#         plt.ylabel('CV%')
#         plt.legend

ax2 = ax.twinx()
# lns1 = df_norm['time spent'].plot(ax=ax2, c='g')
lns1 = ax2.plot(df_norm['time spent'], c='k')
ax2.set_ylabel('time (s)')
# lns2 = df_norm['width CV'].plot(ax=ax)
lns2 = ax.plot(df_norm['width CV'])
lns4 = ax.plot(df_norm['min-x CV'])
lns5 = ax.plot(df_norm['mode CV'])

# lns3 = df_norm['height CV'].plot(ax=ax)
lns3 = ax.plot(df_norm['height CV'])
ax.set_ylabel('CV%')

lns = lns1+lns4+lns5+lns2+lns3
labs = [l.get_label() for l in lns]
a = ax.legend(lns, labs, loc=9)
a.draggable()
plt.tight_layout()
ax.set_xlabel('simulation time (h)')
# print "\n", df

plt.show()