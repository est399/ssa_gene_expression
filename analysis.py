# Author: Nik Rom (n.rom@ncl.ac.uk / est399@me.com)
# Work period: 1st March - 25th August, 2017

from __future__ import division
from analysis_functions import *
import pandas as pd
from pylab import *
import pylab as plt
from shapely.geometry import *
from floatrange import *
import geopandas as gpd
import itertools
from scipy.stats import variation
# from radar_chart import Radar
import os
from detect_peaks import detect_peaks


# --------------------------*******---------------------------
# Note for the future:
# Perhaps add a way of scanning the data folder for the files and give option for analysing
# that specific data. Would be mint, if running from terminal
# --------------------------*******---------------------------


with open("/Users/nikitarom/Bitbucket/SSA_Gene_Expression/output/combbo3/k1_k_1_k5sss_T20.csv",
          'r') as csvfile:
    names = ['param', 'Trange', 'Prange', 'run_n']
    data_initial = pd.read_csv(csvfile, names=names)

# --------<< Pre-process data to get normalised and values of interest par_df >>----------
(data_df, par_df) = process_data(data_initial)
# >> Pull out individual parameters >>--------
# dict_data['data'] = data_df
# dict_par['data'] = par_df
# k_index_dict(dict_par, 0, 0.005)
# k_index_dict(dict_data, 0, 0.005)

# --------<< Graphs >>--------

# >> Processing for plotting
# point_dict = df_to_shapely(data_df)                       # convert to Shapely

uniq_param_dict = extract_unique_par(data_initial)         # dictionary of unique parameters


# df_param_dict = df_dic_param_all(par_df, uniq_param_dict)
# par_df_split = split_param(par_df)


# plot_distribution(point_dict, uniq_param_dict, every_nth=0)  # Plot distribution


df_param_dict = heatmap_df(par_df, uniq_param_dict, product=True)       # Prepare parm for heatmap

plot_heatmap(df_param_dict, four_param=True, annot=False)                                # Plot parm heatmap

#   find peaks from data
# for mph in np.linspace(0., 1., 6):
#     for mpd in np.linspace(0., 10., 6):
#         df_peak = plot_peaks(data_initial, uniq_param_dict, mph=mph, mpd=mpd, print_n=True)
# df_peak_dict = {'ppp': df_peak}
# plot_heatmap(df_peak_dict)

plt.show()
