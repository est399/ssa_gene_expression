# Gillespie algorithm based on code by Dr Angel Goni-Moreno (angel.goni-moreno@ncl.ac.uk)
# Adaptation and addition by Nikita Rom (n.rom@ncl.ac.uk / est399@me.com)
# Work period: 1st March - 25th August, 2017

import random as r
import math as ma
import numpy as np
import csv
import pandas as pd
import os
import time
import multiprocessing as mp
from collections import deque


# --<< Simulation and data processing >>--
def gill(k1, k_1, k2, k3, k4, k5, k6, TF, Time):
    """
    Gillespie Algorithm 
    :param k1:  binding rate --------------- molecule^(-1) hour^(-1) ------ interval [0.005 - 5.0]
    :param k_1: unbinding rate ------------- molecule^(-1) hour^(-1) ------ interval [1.0 - 100.0]
    :param k2:  transcription rate --------- hour^(-1) -------------------- interval [100.0 - 1000.0]
    :param k3:  translation rate ----------- hour^(-1) -------------------- interval [10.0 - 120.0]
    :param k4:  mRNA degradation rate ------ hour^(-1) -------------------- interval [6.0 - 14.0]
    :param k5:  protein degradation rate --- hour^(-1) -------------------- interval [0.1 - 3.0]
    :param k6:  basal transcription rate --- hour^(-1) -------------------- interval [10.0 - 100.0] 
    :param TF:  Number of Transcription Factor molecules
    :param Time: Simulated time for the Gillespie Algorithm (max time)
    :return: list of time-points (trange) and protein concentrations (prange)
    """

    tmax = Time

    t = -3.0
    P = 1.0
    P_A = 0.0
    m = 0.0
    p = 0.0
    trange = []
    prange = []
    timestarted = time.time()

    while t < tmax:
        # all reactions:
        a = [k1 * P * TF, k_1 * P_A, k2 * P_A, k4 * m, k3 * m, k5 * p, k6 * P]
        a0 = sum(a)
        r1 = r.random()
        tau = -ma.log(r1) / a0
        t = t + tau
        r2 = r.random()
        acumsum = np.cumsum(a) / a0
        chosen_reaction = min([i for i in range(len(a)) if acumsum[i] >= r2])
        if chosen_reaction == 0:
            TF -= 1;
            P -= 1;
            P_A += 1;
        if chosen_reaction == 1:
            TF += 1;
            P += 1;
            P_A -= 1;
        if chosen_reaction == 2:
            m += 1;
        if chosen_reaction == 3:
            m -= 1;
        if chosen_reaction == 4:
            p += 1;
        if chosen_reaction == 5:
            p -= 1;
        if chosen_reaction == 6:
            m += 1
        # generate time and protein list
        if t > 1:
            trange.append(t)
            prange.append(p)
    # print ("gille {} ".format(time.time() - timestarted))

    return (trange, prange)


def list_for_uniform(trange, prange):
    """
    Turn the Gillespie output into a time-uniform line
    :param trange: list of time-points
    :param prange: protein concentrations
    :return: new lists where the time increases regularly from time[n] to time[n+1]
    """
    trangeX = []
    prangeX = []
    t_element = round(trange.pop(0), 2)
    p_element = round(prange.pop(0), 2)
    trangeX.append(t_element)
    prangeX.append(p_element)
    timestarted = time.time()
    while trange != []:
        t_element = trange[0]
        p_element = prange[0]
        if round(t_element, 2) == round(trangeX[-1], 2):
            trange.pop(0)
            prange.pop(0)
        elif round(t_element, 2) == round(trangeX[-1] + 0.01, 2):
            trangeX.append(t_element)
            prangeX.append(p_element)
            trange.pop(0)
            prange.pop(0)
        elif round(t_element, 2) > round(trangeX[-1] + 0.01, 2):
            trangeX.append(round(trangeX[-1] + 0.01, 2))
            prangeX.append(prangeX[-1])
        else:
            print t_element, trangeX[-1], exit()
    # print ("list_uniform {} ".format(time.time() - timestarted))

    return trangeX, prangeX


def list_for_histogram(prangeX, sample_interval):
    """
    prepare the lists for plotting the probability distributions
    :param : the protein time-based data
    :return: two lists, one for protein numbers (intervals) and one for probability
    """

    list2 = []
    list1 = []
    for i in range(int(max(prangeX) / sample_interval)):
        number = 0
        for j in range(sample_interval):
            number += prangeX.count(sample_interval * i + j)
        list2.append(float(number) / float(len(prangeX)))
        list1.append(i)

    return list1, list2


def distribution(prangeX, sample_interval):
    """
    Prepare the lists for plotting the flow cytometry simulations
    :param prangeX: the protein time-based data
    :param sample_interval: step for data sampling
    :return: two lists, one for protein numbers (intervals) and one for sample counts
    """
    list1 = []
    list2 = []
    timestarted = time.time()

    for i in range(int(max(prangeX) / sample_interval)):
        number = 0
        for j in range(sample_interval):
            number += prangeX.count(sample_interval * i + j)
        list1.append(i * sample_interval)
        list2.append(float(number))
    # print ("distribution {} ".format(time.time() - timestarted))
    return (list1, list2)


def range_val(start, end, length):
    """
    Writes a list length 'length' of float values from start till end value rounded to 2.
    :param start: starting value
    :param end: ending value
    :param length: length of the list
    :return: list of comb
    """
    lll = np.linspace(start, end, length, True).tolist()
    combi = [round(lll[n], 2) for n in range(len(lll))]

    return combi


# --<< Setting the environment and writing >>--
def specify_type(cc_list):
    """
    checks for values in the list of parameters that are longer than 1, thus of interest and
    joins them into a string.
    
    :param cc_list: a list of parameters [k1, k_1, k2, k3, k4, k5, k6]
    :return: a string with the values that are changed in the simulation 
    """
    cc_title = ['k1', 'k_1', 'k2', 'k3', 'k4', 'k5', 'k6']
    cc_type = []
    for cc in range(len(cc_list)):
        if len(cc_list[cc]) > 1:
            cc_type.append(cc_title[cc])

    if cc_type == []:
        c_type = 'standard'
    else:
        c_type = '_'.join(cc_type)

    return c_type


def check_set_dir():
    """
    Test if directory exists and set it as the directory for output files and log files
    :return: path to the subdirectory
    """
    cwd = os.getcwd()
    cwd_out = '{}/output'.format(cwd)

    if not os.path.exists(cwd_out):
        os.makedirs(cwd_out)

    return cwd_out


def check_run_n(mydir, type, time):
    """
    Check if previous runs exists, if so add one to counter and return 
    :param mydir: working directory
    :param type: name of the simulation
    :param time: length of the simulation
    :return: new run number
    """
    try:
        myfile = '{0}/{1}_T{2}.csv'.format(mydir,type, time)
        with open(myfile, 'r') as csvfile:
            lastrow = deque(csv.reader(csvfile), 1)[0]
            run_n = (int(lastrow[-1]))
            print "Last run number is {}, proceeding...".format(run_n)
    except IOError:
        print "No previous run was found; set run_n to 0"
        run_n = 0
    return run_n


def write_range_csv(type, mydir,
                    Trange, Prange, time, param_list, run_n):
    """
    Writes results from Gillespie simulation to CSV file 
    :param type: name of the simulation (str)
    :param mydir: Working directory set by the 'check_set_dir' function
    :param Trange: list of time-points
    :param Prange: list of protein either instances, or concentrations
    :param run_n: number of the run (if applicable)
    :param time: length of the simulation
    :param [k1, k_1, k2, k3, k4, k5, k6, TF]: kinetic rates and transcription factor 
    :return: csv file contained in the directory with the output written in csv format
    """
    param_list = [param_list] * len(Trange)
    run_n_list = [run_n] * len(Trange)

    zip_lists = list(zip(param_list, Trange, Prange, run_n_list))
    pd_data = pd.DataFrame(data=zip_lists, columns=['Param', 'Trange', 'Prange', 'Run_n'])
    myfile = '{0}/{1}_T{2}.csv'.format(mydir, type, time)

    with open(myfile, 'a') as csvfile:
        pd_data.to_csv(csvfile, index=False, header=False)


# --<< Running the simulation with respect to multiprocessing >>--
def listener(c, q):
    """
    Retrieves results from the queue and writes them to file one by one
    :param c: counter
    :param q: results stored in multiprocessing.Queue, contains:
            (Type, mydir, Trange, Prange, time, param_list, run_n, time_spent)
    :return: writes results to file, prints out time spent, run_number and number of the simulation 
    in an organised and fancy manner
    """
    while 1:
        m = q.get()
        if m == 'kill':
            break

        (type, mydir, Trange, Prange, time, param_list, run_n, time_spent) = m
        write_range_csv(type=type, mydir=mydir, Trange=Trange, Prange=Prange, time=time,
                        param_list=param_list, run_n=run_n)
        print ("-{1}-{2}\t- time spent {0} sec---".format(time_spent, run_n, c))
        c += 1


def worker(arg, q):
    """
    Worker for the multiprocessor, runs simulations.
    :param arg: an argument containing all the values necessary for the worker to run the simulation:
            (param_list, TF, Time, mydir, run_n, sample_interval, Type)
    :param q: results stored in multiprocessing.Queue, contains:
            (Type, mydir, Trange, Prange, time, param_list, run_n, time_spent)
    :return: res - results stored in one argument. 
    """
    timestart = time.time()

    (param_list, TF, Time, mydir, run_n, sample_interval, type) = arg
    [k1, k_1, k2, k3, k4, k5, k6] = param_list
    (trange, prange) = gill(k1, k_1, k2, k3, k4, k5, k6, TF, Time)
    (trangeX, prangeX) = list_for_uniform(trange, prange)
    (list1b, list2b) = distribution(prangeX, sample_interval)

    time_spent = round((time.time() - timestart), 2)
    res = ('{}'.format(type), mydir, list1b, list2b, Time, param_list, run_n, time_spent)
    q.put(res)
    return res


def multiprocess_run(comb, TF, Time, mydir, run_n, sample_interval, type, processors_number=False):
    """
    Initialises the multiprocessing for Gillespie algorithm. First activates the listener (a bottleneck
    for results) through initialising Queue, then runs Gil based pulling combinations of parameters from
    predefined list and writes results both to queue and 'jobs' list. Finally, retrieves jobs and kills
    the listener. Closes and joins the pool. 
    (NB! pool.join() is essential for successful multiprocessing, gives odd results when not specified)
    :param comb: a list of combinations
    :param TF: transcription factor number
    :param Time: time of the simulation
    :param mydir: Working directory set by the 'check_set_dir' function
    :param run_n: number of the run (if applicable)
    :param sample_interval: step for data sampling
    :param type: name of the simulation (str) 
    :param processors_number: Number of processors at work, if not specified - half of the available
    :return: writes the results of the simulation to a file (via listener)
    """
    c = 0
    manager = mp.Manager()
    q = manager.Queue()

    if processors_number is False:
        pool = mp.Pool(mp.cpu_count()/2)
    else:
        pool = mp.Pool(processors_number)

    pool.apply_async(listener, (c, q,))
    jobs = []
    for i in range(len(comb)):
        param_list = comb[i]
        arg = (param_list, TF, Time, mydir, run_n, sample_interval, type)

        job = pool.apply_async(worker, (arg, q))
        jobs.append(job)

    for job in jobs:
        job.get()

    q.put('kill')
    pool.close()
    pool.join()


# --<< Additional, left out functions >>--
def write_log(type, mydir, time, TIME_taken, param_list, run_n):
        """
        Writes log of time taken to complete task into a file
        :param type: name of the simulation (str)
        :param mydir: Working directory set by the 'check_set_dir' function
        :param time: length of the simulation
        :param TIME_taken: Length of time required for simulating the system
        :param [k1, k_1, k2, k3, k4, k5, k6, TF]: kinetic rates and transcription factor 
        :param run_n: number of the run (if applicable)
        :return: csv file containing the log to a LOG subdirectory 
        """
        TIME_taken = round(TIME_taken, 2)
        myfile = '{0}/log/LOG_T{1}.csv'.format(mydir, time)
        zip_lists = list(zip([param_list], [run_n], [type], [TIME_taken]))
        pd_data = pd.DataFrame(data=zip_lists, columns=['Param', 'Run_n', 'Type', 'Time'])

        with open(myfile, 'a') as csvfile:
            pd_data.to_csv(csvfile, index=False, header=False)


def comb_run_write(comb, TF, Time, mydir, run_n, sample_interval, type):
    """
    Runs Gillespie algorithm with a combination of input parameters and writes them to file
    :param comb: combination of parameters
    :param TF: transcription factor number
    :param Time: length of the simulation
    :param mydir: Working directory set by the 'check_set_dir' function
    :param run_n: number of the run
    :param sample_interval: step for data sampling
    :param type: name of the simulation (str)
    :return: writes the output to file and prints out number of the run and time
    """
    timestart = time.time()

    k1 = comb[0]  # [0.005 - 5.0]
    k_1 = comb[1]  # [1.0 - 100.0]
    k2 = comb[2]  # [100.0 - 1000.0]
    k3 = comb[3]  # [10.0 - 120.0]
    k4 = comb[4]  # [6.0 - 14.0]
    k5 = comb[5]  # [0.1 - 3.0]
    k6 = comb[6]  # [10.0 - 100.0]

    param_list = [k1, k_1, k2, k3, k4, k5, k6]
    (trange, prange) = gill(k1, k_1, k2, k3, k4, k5, k6, TF, Time)
    (trangeX, prangeX) = list_for_uniform(trange, prange)

    (list1b, list2b) = distribution(prangeX, sample_interval)
    write_range_csv('{}'.format(type), mydir, list1b, list2b,
                    Time, param_list, run_n)

    print ("-{1}- time spent {0} sec---".format(round((time.time() - timestart), 2), run_n))


def create_dic_distribution(group_by, sample_interval):
    """
    Creates a dictionary after calculating the uniform distribution
    from protein occurrences and time points.
    :param group_by: group_by('param') containing TP
    :param sample_interval: distribution sampling distance
    :return: dictionary containing OP
    """
    time_start = time.time()
    OP_dict = {}
    for i, group in group_by:
        templist = list(group['Prange'])
        (list1b, list2b) = distribution(templist, sample_interval)
        temp = (list1b, list2b)
        OP_dict['{}'.format(i)] = temp
        print ("time for distribution = {}".format((time.time()) - time_start))
    return OP_dict
