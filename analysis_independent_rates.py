# Author: Nik Rom (n.rom@ncl.ac.uk / est399@me.com)
# Work period: 1st March - 25th August, 2017

from __future__ import division
from analysis_functions import *
import pandas as pd
from pylab import *
import pylab as plt
from shapely.geometry import *
from floatrange import *
import geopandas as gpd
import itertools
from scipy.stats import variation
# from radar_chart import Radar
import os
from detect_peaks import detect_peaks
import os


mydir = '/Users/nikitarom/Bitbucket/SSA_Gene_Expression/output/independent/'
files_list = os.listdir(mydir)

data_dict = {}

for csvfile in files_list:
    with open(mydir+csvfile, 'r') as csv:
        names = ['param', 'Trange', 'Prange', 'run_n']
        data_temporal = pd.read_csv(csv, names=names)
        titles = ['k1', 'k_1', 'k2', 'k3', 'k4', 'k5', 'k6']
        name = [n for n in titles if n[:2] == csvfile[:2]][0]

        data_dict[name] = data_temporal


data_n_dict = {}
data_point_dict = {}
data_uniq_p_dict = {}

df = pd.DataFrame()

for k, v in sorted(data_dict.iteritems()):
    uniq_param_dict = extract_unique_par(v)

    (data_df, par_df) = process_data(data_initial=v)
    point_dict = df_to_shapely(data_df)

    kkk = pd.Series([k]*len(par_df))
    df_kkk = pd.DataFrame(pd.concat([kkk, par_df], axis=1))
    df = df.append([df_kkk])
    (area_dict, bounds_dict) = plot_distribution(point_dict, uniq_param_dict, every_nth=0)
    # plot_par_df(par_df, k, label=False)

    data_n_dict[k] = (data_df, par_df)
    data_point_dict[k] = point_dict
    data_uniq_p_dict[k] = uniq_param_dict

    summ = []
    for col in data_df.columns:
        val = data_df[col]
        summ.append(sum(val * val.index))
    # print k, variation(area_dict.values())*100
    print k, max(summ)


def find_variation(individual=False):
    if individual is False:
        for n in df.columns[2:]:
            df[n] = (df[n] - df[n].min()) / (df[n].max() - df[n].min())

    df_group = df.groupby(0)

    df_var = pd.DataFrame(columns=df.columns[2:], index=sorted(df_group.groups))

    for key, item in df_group:
        for col in item.columns[2:]:
            # print col, variation(item[col])*100
            df_var[col][key] = round(variation(item[col])*100, 3)
    print df_var
# find_variation(individual=False)

# a = plt.figlegend()
# a.draggable()
# plt.tight_layout()
plt.show()
