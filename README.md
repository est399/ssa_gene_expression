# SSA for Gene Expression README #

#### Version 0.1.0

### Description

The following repository contains scripts that were implemented for the purposes of the research project under the title:
**Making Sense of Noise in Gene Expression: Effects of Stochastic Rate Variation on Gene Expression Levels**

### System Requirements
* Python 2.7

### Software Requirements
* Python Standard Library
* SciPy ecosystem: numpy, matplotlib, pandas
* Seaborn
* Shapely

### Contained Files and Folders
#### 1.1 ssa_functions.py
> This file containes all the functions for running SSA (Gillespie) algorithm and following data transformation,
> as well, as writing the results to file. In addition, handlers for multiprocessing are held here.

#### 1.2 run.py
> This script calls on the 'ssa_function.py' to perform the simulation. It also contains the handlers for producing 
> rate combination.

#### 2.1 analysis_function.py
> The file containing all the functions for performing the further analysis.

#### 2.2 analysis_independent_rates.py
> Script calls the 'analysis_function.py' file to perform analysis on the folder containing the results from
> simulations of indepent rates.

#### 2.3 analysis.py
> Script calls the 'analysis_function.py' file to perform general analysis.

#### 3.0 test_time_variation (folder):
##### 3.1 var_times_write.py
> Script for obtaining data for variation analysis across different simulation times
##### 3.1 var_times_analysis.py
> Script for analysing the variation


#### 4.0 detect_peaks.py
> An experimental script for finding peaks. (ignore)